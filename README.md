# features
1. assume all data are in csv format
2. opencsv open-source has been applied everywhere to manipulate the csv files, it's configurable as well
3. no db operations and jpa needed
4. tests are added, including integration tests and unit tests
5. multiple Rest APIs provided
6. pipeline has added in this repo for easy observing the status of each commits
7. all write related methods are thread-safe, which all invoke write method in java.io.Writer. This is supposed to well support multiple users operations and have no issue regarding concurrency.

# what we could do:
1. could read records from csv file and copy to beans automatically
2. data could save in list, which is easy for further use, like analysis
3. could save records in csv file from beans automatically
4. could append record to csv file
5. could remove record in the specific line
7. could seek records by line
8. could remove all records except header
9. could upload a bulk of data set or single csv file
10. could query from the specific file or all files if specified file doesn't exist
11. could add one record to the specified file
12. if some csv file provided and has different sorts of fields, to use this code, you just need to define a bean and a service with corresponding methods.
```java
//xxxBean
class xxxBean extends CsvBean{
    ...
    @Override
    public String toString() {
        return this.toStringHelper();
    }
}

// xxxService
class xxxService{
    
}
```

# how to run

- git clone this project or download it
- run application by mvn spring:boot:run
- run test by mvn test, or you could check the status of pipeline


# further
1. if more data provided and more complex cases coming, we probably need to use some nosql and jpa tech for data persistence. However, right now for this project, I would like not to use jpa. it's quite heavy.
2. considering I have provided more flexible bean way to handle csv datasets, and the operations of write/read  are thread-safe, this completely meets our requirements.
