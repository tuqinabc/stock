package org.qtcoding.stock.demo.controller;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.qtcoding.stock.demo.common.UploadFileResponse;
import org.qtcoding.stock.demo.dto.StockDto;
import org.qtcoding.stock.demo.entity.Stock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;

import java.io.File;
import java.util.List;
import java.util.stream.Stream;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class StockControllerTest {

    final private String URL = "http://localhost:8080";
    @Autowired
    private TestRestTemplate restTemplate;

    @BeforeEach
    void setup() {
        Stream.of("data/test_0.data", "data/test_1.data", "data/test_2.data").forEach(f -> {
            File file = new File(f);
            if (file.exists()) {
                file.delete();
            }
        });

    }

    @AfterEach
    void deletefiles() {
        Stream.of("data/test_0.data", "data/test_1.data", "data/test_2.data").forEach(f -> {
            File file = new File(f);
            if (file.exists()) {
                file.delete();
            }
        });

    }

    @Test
    void test_upload_single_file() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        LinkedMultiValueMap<String, Object> params = new LinkedMultiValueMap<>();
        params.add("file", new org.springframework.core.io.ClassPathResource("test_0.data"));
        HttpEntity<LinkedMultiValueMap<String, Object>> httpEntity = new HttpEntity<>(params, headers);
        ResponseEntity<UploadFileResponse> responseEntity = restTemplate.postForEntity(URL + "/uploadFile", httpEntity, UploadFileResponse.class);
        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertEquals("http://localhost:8080/data/test_0.data", responseEntity.getBody().getFileDownloadUri());
        Assertions.assertEquals("test_0.data", responseEntity.getBody().getFileName());
    }

    @Test
    void test_upload_upload_multiple_files() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        LinkedMultiValueMap<String, Object> params = new LinkedMultiValueMap<>();
        params.add("files", new org.springframework.core.io.ClassPathResource("test_0.data"));
        params.add("files", new org.springframework.core.io.ClassPathResource("test_1.data"));
        params.add("files", new org.springframework.core.io.ClassPathResource("test_2.data"));
        HttpEntity<LinkedMultiValueMap<String, Object>> httpEntity = new HttpEntity<>(params, headers);
        ResponseEntity<List> responseEntity = restTemplate.postForEntity(URL + "/uploadMultipleFiles", httpEntity, List.class);
        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertEquals(3, responseEntity.getBody().size());
    }

    @Test
    void test_query_by_stock_ticker_with_specific_file() {
        ResponseEntity<List> responseEntity = restTemplate.getForEntity(URL + "/dow_jones_index/AA", List.class);
        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertEquals(25, responseEntity.getBody().size());
    }

    @Test
    void test_query_by_stock_ticker_with_all_file() {
        ResponseEntity<List> responseEntity = restTemplate.getForEntity(URL + "/dow_jones_/AA", List.class);
        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertEquals(51, responseEntity.getBody().size());
    }

    @Test
    void test_append_record_to_csv_file() {
        HttpHeaders headers = new HttpHeaders();
        StockDto dto = new StockDto();
        dto.setFilePath("dow_jones_index");
        Stock stock = new Stock();
        stock.setQuarter("4");
        stock.setStock("TT");
        dto.setStock(stock);
        HttpEntity<StockDto> httpEntity = new HttpEntity(dto, headers);
        ResponseEntity<List> responseEntity = restTemplate.postForEntity(URL + "/stock", httpEntity, List.class);
        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }
}