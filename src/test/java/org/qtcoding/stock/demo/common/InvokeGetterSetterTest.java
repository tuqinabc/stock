package org.qtcoding.stock.demo.common;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class InvokeGetterSetterTest {

    @Test
    void test_invoke_setter() {
        User user = new User(9, "Emma Watson 09", "emma.watson@example.com", "UK");
        InvokeGetterSetter.invokeSetter(user, "name", "Peter Quinn");
        Assertions.assertEquals("Peter Quinn", user.getName());
    }

    @Test
    void test_invoke_getter() {
        User user = new User(9, "Emma Watson 09", "emma.watson@example.com", "UK");
        Object obj = InvokeGetterSetter.invokeGetter(user, "name");
        Assertions.assertEquals("Emma Watson 09", obj.toString());
    }

}