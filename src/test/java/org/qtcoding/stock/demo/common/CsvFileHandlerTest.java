package org.qtcoding.stock.demo.common;

import org.junit.jupiter.api.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class CsvFileHandlerTest {
    private static String test_file = "src/test/resources/test.data";
    private int count = 0;

    @BeforeAll
    void setUp() throws IOException {
        File file = new File(test_file);
        if (file.exists()) {
            file.delete();
        }
        file.createNewFile();
        FileWriter w = new FileWriter(file);
        String header = "id, name, email, country".replace("\"", "");
        w.write(header);
        w.close();
    }

    @Test
    @Order(1)
    void test_append_record_in_csv() {
        String[] record01 = "1,Emma Watson 01,emma.watson@example.com,UK".split(",");
        String[] record02 = "2,Emma Watson 02,emma.watson@example.com,UK".split(",");
        CsvFileHandler.appendRecordInCsvFile(test_file, record01);
        int lines = CsvFileHandler.appendRecordInCsvFile(test_file, record02);
        Assertions.assertEquals(3, lines);// including header
    }

    @Test
    @Order(2)
    void test_seek_one_record_in_csv() {
        String[] res = CsvFileHandler.seekRecordInCsvFile(test_file, 2);
        String[] expect = "2,Emma Watson 02,emma.watson@example.com,UK".split(",");
        Assertions.assertArrayEquals(expect, res);
    }

    @Test
    @Order(3)
    void test_delete_one_record_in_csv() {
        int lines = CsvFileHandler.deleteRecordInCsvFile(test_file, 1);
        Assertions.assertEquals(2, lines);// including header
    }

    @Test
    @Order(4)
    void test_delete_all_records_in_csv() {
        int lines = CsvFileHandler.keepHeaderOnlyInCsvFile(test_file);
        Assertions.assertEquals(1, lines);// including header
    }

    @Test
    @Order(5)
    void test_write_csv_from_bean() {
        User user = new User(9, "Emma Watson 09", "emma.watson@example.com", "UK");
        CsvFileHandler.writeCsvFileFromBean(test_file, user);
        String[] res = CsvFileHandler.seekRecordInCsvFile(test_file, 1);
        String[] expect = "9,Emma Watson 09,emma.watson@example.com,UK".split(",");
        Assertions.assertArrayEquals(expect, res);
    }

    @Test
    @Order(6)
    void test_read_from_csv_return_bean() {
        CsvTransfer<User> cst = CsvFileHandler.readCsvFileToBean(test_file, User.class);
        Assertions.assertEquals(1, cst.getCsvList().size());
        Assertions.assertEquals(9, cst.getCsvList().get(0).getId());
        Assertions.assertEquals("Emma Watson 09", cst.getCsvList().get(0).getName());
        Assertions.assertEquals("emma.watson@example.com", cst.getCsvList().get(0).getEmail());
        Assertions.assertEquals("UK", cst.getCsvList().get(0).getCountryCode());
    }
}