package org.qtcoding.stock.demo.common;

import com.opencsv.bean.CsvBindByName;
import org.qtcoding.stock.demo.entity.CsvBean;

public class User extends CsvBean {
    @CsvBindByName
    public int id;
    @CsvBindByName
    public String name;
    @CsvBindByName
    public String email;
    @CsvBindByName(column = "country")
    public String countryCode;

    public User(int id, String name, String email, String countryCode) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.countryCode = countryCode;
    }

    public User() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return this.toStringHelper();
    }
}
