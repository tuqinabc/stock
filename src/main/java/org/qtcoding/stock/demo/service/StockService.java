package org.qtcoding.stock.demo.service;

import org.qtcoding.stock.demo.entity.Stock;
import org.qtcoding.stock.demo.common.CsvFileHandler;
import org.qtcoding.stock.demo.common.CsvTransfer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StockService {
    @Autowired
    private StoreFileStorageService storeFileStorageService;

    public List<Stock> queryStockItemByStock(String fileName, String stock) {
        final List<Stock> stocks = new ArrayList();
        final String file = storeFileStorageService.loadFile(fileName);
        final CsvTransfer<Stock> csvTransfer = CsvFileHandler.readCsvFileToBean(file, Stock.class);
        csvTransfer.getCsvList().stream().filter(f -> f.getStock().trim().equals(stock)).forEach(stocks::add);
        return stocks;
    }

    public List<Stock> queryStockItemByStockFromAll(String stock) {
        final List<Stock> stocks = new ArrayList();
        final List<String> files = storeFileStorageService.loadAllFiles();
        files.stream().forEach(f -> {
            CsvTransfer<Stock> cst = CsvFileHandler.readCsvFileToBean(f, Stock.class);
            cst.getCsvList().stream().filter(r -> r.getStock().trim().equals(stock)).forEach(stocks::add);
        });
        return stocks;
    }

    public void addStockItem(String fileName, Stock stockItem) {
        final String file = storeFileStorageService.loadFile(fileName);
        CsvFileHandler.writeCsvFileFromBean(file, stockItem);
    }
}
