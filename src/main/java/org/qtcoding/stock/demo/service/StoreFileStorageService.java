package org.qtcoding.stock.demo.service;

import org.qtcoding.stock.demo.exception.StockFileStorageException;
import org.qtcoding.stock.demo.common.FileStorageProperties;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

@Service
public class StoreFileStorageService {

    private final Path fileStorageLocation;


    public StoreFileStorageService(FileStorageProperties fileStorageProperties) {
        this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir())
                .toAbsolutePath().normalize();
        try {
            Files.createDirectories(this.fileStorageLocation);
        } catch (Exception ex) {
            throw new StockFileStorageException("Could not create the directory where the uploaded files will be stored.", ex);
        }
    }

    public String storeFile(MultipartFile file) {
        // Normalize file name
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

        try {
            // Check if the file's name contains invalid characters
            if (fileName.contains("..")) {
                throw new StockFileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
            }

            // Copy file to the target location (Replacing existing file with the same name)
            Path targetLocation = this.fileStorageLocation.resolve(fileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

            return fileName;
        } catch (IOException ex) {
            throw new StockFileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }

    public String loadFile(String fileName) {
        Path filePath = this.fileStorageLocation.resolve(fileName + ".data").normalize();
        if (filePath.toFile().exists()) {
            return filePath.toString();
        }
        return null;
    }

    public List<String> loadAllFiles() {
        Path folderPath = this.fileStorageLocation.normalize();
        List<String> filePaths = new ArrayList<>();
        Stream.of(folderPath.toFile().listFiles()).forEach(f -> filePaths.add(f.getAbsolutePath()));
        return filePaths;
    }
}