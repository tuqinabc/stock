package org.qtcoding.stock.demo.dto;

import org.qtcoding.stock.demo.entity.Stock;

public class StockDto {
    private String filePath;
    private Stock stock;

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }
}
