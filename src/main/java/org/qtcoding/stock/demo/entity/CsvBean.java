package org.qtcoding.stock.demo.entity;

import org.qtcoding.stock.demo.common.InvokeGetterSetter;

import java.lang.reflect.Field;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class CsvBean {
    protected String toStringHelper() {
        final Field[] fields = this.getClass().getDeclaredFields();
        final String str = Stream.of(fields).map(f ->{
                    Object obj = InvokeGetterSetter.invokeGetter(this, f.getName());
                    if (obj==null) {
                        return "null";
                    } else {
                        return obj.toString();
                    }
                }).collect(Collectors.joining(","));
        return str;
    }
}