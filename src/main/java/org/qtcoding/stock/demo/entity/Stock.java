package org.qtcoding.stock.demo.entity;


import com.opencsv.bean.CsvBindByName;

public class Stock extends CsvBean {
    //the yearly quarter (1 = Jan-Mar; 2 = Apr=Jun).
    @CsvBindByName(column = "quarter")
    private String quarter;
    //the stock symbol (see above)
    @CsvBindByName(column = "stock")
    private String stock;
    //the last business day of the work (this is typically a Friday)
    @CsvBindByName(column = "date")
    private String date;
    //the price of the stock at the beginning of the week
    @CsvBindByName(column = "open")
    private String open;
    //the highest price of the stock during the week
    @CsvBindByName(column = "high")
    private String high;
    //the lowest price of the stock during the week
    @CsvBindByName(column = "low")
    private String low;
    //the price of the stock at the end of the week
    @CsvBindByName(column = "close")
    private String close;
    //the number of shares of stock that traded hands in the week
    @CsvBindByName(column = "volume")
    private String volume;
    //the percentage change in price throughout the week
    @CsvBindByName(column = "percent_change_price")
    private String percent_change_price;
    //the percentage change in the number of shares of stock that traded hands for this week compared to the previous week
    @CsvBindByName(column = "percent_change_volume_over_last_wk")
    private String percent_change_volume_over_last_wek;
    //the number of shares of stock that traded hands in the previous week
    @CsvBindByName(column = "previous_weeks_volume")
    private String previous_weeks_volume;
    //the opening price of the stock in the following week
    @CsvBindByName(column = "next_weeks_open")
    private String next_weeks_open;
    // the closing price of the stock in the following week
    @CsvBindByName(column = "next_weeks_close")
    private String next_weeks_close;
    //the percentage change in price of the stock in the following week days_to_next_dividend: the number of days until the next dividend
    @CsvBindByName(column = "percent_change_next_weeks_price")
    private String percent_change_next_weeks_price;
    // the percentage of return on the next dividend
    @CsvBindByName(column = "days_to_next_dividend")
    private String days_to_next_dividend;
    // the percentage of return on the next dividend
    @CsvBindByName(column = "percent_return_next_dividend")
    private String percent_return_next_dividend;

    public String getQuarter() {
        return quarter;
    }

    public void setQuarter(String quarter) {
        this.quarter = quarter;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getOpen() {
        return open;
    }

    public void setOpen(String open) {
        this.open = open;
    }

    public String getHigh() {
        return high;
    }

    public void setHigh(String high) {
        this.high = high;
    }

    public String getLow() {
        return low;
    }

    public void setLow(String low) {
        this.low = low;
    }

    public String getClose() {
        return close;
    }

    public void setClose(String close) {
        this.close = close;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String getPercent_change_price() {
        return percent_change_price;
    }

    public void setPercent_change_price(String percent_change_price) {
        this.percent_change_price = percent_change_price;
    }

    public String getPercent_change_volume_over_last_wek() {
        return percent_change_volume_over_last_wek;
    }

    public void setPercent_change_volume_over_last_wek(String percent_change_volume_over_last_wek) {
        this.percent_change_volume_over_last_wek = percent_change_volume_over_last_wek;
    }

    public String getPrevious_weeks_volume() {
        return previous_weeks_volume;
    }

    public void setPrevious_weeks_volume(String previous_weeks_volume) {
        this.previous_weeks_volume = previous_weeks_volume;
    }

    public String getNext_weeks_open() {
        return next_weeks_open;
    }

    public void setNext_weeks_open(String next_weeks_open) {
        this.next_weeks_open = next_weeks_open;
    }

    public String getNext_weeks_close() {
        return next_weeks_close;
    }

    public void setNext_weeks_close(String next_weeks_close) {
        this.next_weeks_close = next_weeks_close;
    }

    public String getPercent_change_next_weeks_price() {
        return percent_change_next_weeks_price;
    }

    public void setPercent_change_next_weeks_price(String percent_change_next_weeks_price) {
        this.percent_change_next_weeks_price = percent_change_next_weeks_price;
    }

    public String getDays_to_next_dividend() {
        return days_to_next_dividend;
    }

    public void setDays_to_next_dividend(String days_to_next_dividend) {
        this.days_to_next_dividend = days_to_next_dividend;
    }

    public String getPercent_return_next_dividend() {
        return percent_return_next_dividend;
    }

    public void setPercent_return_next_dividend(String percent_return_next_dividend) {
        this.percent_return_next_dividend = percent_return_next_dividend;
    }

    @Override
    public String toString() {
        return this.toStringHelper();
    }
}
