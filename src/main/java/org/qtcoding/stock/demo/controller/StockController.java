package org.qtcoding.stock.demo.controller;

import com.google.common.base.Strings;
import org.qtcoding.stock.demo.dto.StockDto;
import org.qtcoding.stock.demo.entity.Stock;
import org.qtcoding.stock.demo.service.StockService;
import org.qtcoding.stock.demo.service.StoreFileStorageService;
import org.qtcoding.stock.demo.common.UploadFileResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class StockController {
    private static final Logger logger = LoggerFactory.getLogger(StockController.class);

    @Autowired
    private StoreFileStorageService storeFileStorageService;
    @Autowired
    private StockService stockService;

    // upload a bulk data set
    // single file
    @PostMapping("/uploadFile")
    public UploadFileResponse uploadFile(@RequestParam("file") MultipartFile file) {
        logger.debug("Start of uploadFile()");
        String fileName = storeFileStorageService.storeFile(file);
        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("data/")
                .path(fileName)
                .toUriString();
        return new UploadFileResponse(fileName, fileDownloadUri,
                file.getContentType(), file.getSize());
    }

    // multiple files
    @PostMapping("/uploadMultipleFiles")
    public List<UploadFileResponse> uploadMultipleFiles(@RequestParam("files") MultipartFile[] files) {
        logger.debug("Start of uploadMultipleFiles()");
        return Arrays.asList(files)
                .stream()
                .map(file -> uploadFile(file))
                .collect(Collectors.toList());
    }

    // query for data by stock ticker (e.g. input: AA, would return 25 elements if the only data uploaded were the single data set above)
    @GetMapping("/{filename}/{stock}")
    public List<Stock> queryByStockTicker(@PathVariable String filename, @PathVariable String stock) {
        logger.debug("Start of queryByStockTicker()");
        String resource = storeFileStorageService.loadFile(filename);
        final List<Stock> stocks;
        if (Strings.isNullOrEmpty(resource)) {
            logger.debug("file {} doesn't exist", filename);
            stocks = stockService.queryStockItemByStockFromAll(stock);
        } else {
            logger.debug("loading data from {}", filename);
            stocks = stockService.queryStockItemByStock(filename, stock);
        }
        return stocks;

    }

    // add a new record
    @PostMapping("/stock")
    public void appendRecordToCsvFile(@RequestBody StockDto stockDto) {
        logger.debug("Start of addStockItem()");
        stockService.addStockItem(stockDto.getFilePath(), stockDto.getStock());
    }
}
