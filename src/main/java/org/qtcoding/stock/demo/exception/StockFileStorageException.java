package org.qtcoding.stock.demo.exception;

public class StockFileStorageException extends RuntimeException {
    public StockFileStorageException(String message) {
        super(message);
    }

    public StockFileStorageException(String message, Throwable cause) {
        super(message, cause);
    }
}