package org.qtcoding.stock.demo.exception;

public class StockFileManipulateException extends RuntimeException {
    public StockFileManipulateException(String message) {
        super(message);
    }

    public StockFileManipulateException(String message, Throwable cause) {
        super(message, cause);
    }
}