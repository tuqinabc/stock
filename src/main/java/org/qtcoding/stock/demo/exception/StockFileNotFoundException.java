package org.qtcoding.stock.demo.exception;

public class StockFileNotFoundException extends RuntimeException {
    public StockFileNotFoundException(String message) {
        super(message);
    }

    public StockFileNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}