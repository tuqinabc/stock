package org.qtcoding.stock.demo.common;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import com.opencsv.CSVWriterBuilder;
import com.opencsv.ICSVWriter;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.HeaderColumnNameMappingStrategy;
import org.qtcoding.stock.demo.entity.CsvBean;
import org.qtcoding.stock.demo.exception.StockFileManipulateException;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.List;

public class CsvFileHandler {
    public static <T extends CsvBean> CsvTransfer readCsvFileToBean(String filePath, Class<T> clazz) {
        CsvTransfer<T> csvTransfer = new CsvTransfer();
        HeaderColumnNameMappingStrategy ms
                = new HeaderColumnNameMappingStrategy<>();
        try {
            ms.setType(clazz);
            Reader reader = new FileReader(filePath);
            CsvToBean cb = new CsvToBeanBuilder(reader).withType(clazz)
                    .withMappingStrategy(ms)
                    .build();

            csvTransfer.setCsvList(cb.parse());
            reader.close();

        } catch (Exception e) {
            throw new StockFileManipulateException("Csv File read exception", e);
        }
        return csvTransfer;
    }

    public static <T extends CsvBean> void writeCsvFileFromBean(String filePath, T t) {
        try {
            Writer writer = new FileWriter(filePath, true);
            ICSVWriter csvWriter = new CSVWriterBuilder(writer)
                    .withSeparator(CSVWriter.DEFAULT_SEPARATOR)
                    .withQuoteChar(CSVWriter.NO_QUOTE_CHARACTER)
                    .withEscapeChar(CSVWriter.DEFAULT_ESCAPE_CHARACTER)
                    .withLineEnd(CSVWriter.DEFAULT_LINE_END)
                    .build();

            csvWriter.writeNext(t.toString().split(","));
            writer.close();
        } catch (Exception e) {
            throw new StockFileManipulateException("Csv File write exception", e);
        }
    }

    public static <T extends CsvBean> int keepHeaderOnlyInCsvFile(String filePath) {
        try {
            Reader reader = new FileReader(filePath);
            CSVReader csvReader = new CSVReader(reader);
            List<String[]> allElements = csvReader.readAll();
            String[] header = allElements.get(0);
            allElements.clear();
            allElements.add(header);

            reader.close();
            Writer writer = new FileWriter(filePath);
            CSVWriter csvWriter = new CSVWriter(writer,
                    CSVWriter.DEFAULT_SEPARATOR,
                    CSVWriter.NO_QUOTE_CHARACTER,
                    CSVWriter.NO_ESCAPE_CHARACTER,
                    CSVWriter.DEFAULT_LINE_END);
            csvWriter.writeAll(allElements);
            writer.close();
            return allElements.size();
        } catch (Exception e) {
            throw new StockFileManipulateException("Csv File write/read exception", e);
        }
    }

    public static <T extends CsvBean> String[] seekRecordInCsvFile(String filePath, int line) {
        try {
            Reader reader = new FileReader(filePath);
            CSVReader csvReader = new CSVReader(reader);
            List<String[]> allElements = csvReader.readAll();
            reader.close();
            return allElements.get(line);
        } catch (Exception e) {
            throw new StockFileManipulateException("Csv File write/read exception", e);
        }
    }

    public static <T extends CsvBean> int appendRecordInCsvFile(String filePath, String[] record) {
        try {
            Reader reader = new FileReader(filePath);
            CSVReader csvReader = new CSVReader(reader);
            List<String[]> allElements = csvReader.readAll();
            allElements.add(record);
            reader.close();

            Writer writer = new FileWriter(filePath);
            CSVWriter csvWriter = new CSVWriter(writer, CSVWriter.DEFAULT_SEPARATOR,
                    CSVWriter.NO_QUOTE_CHARACTER,
                    CSVWriter.NO_ESCAPE_CHARACTER,
                    CSVWriter.DEFAULT_LINE_END);

            csvWriter.writeAll(allElements);
            writer.close();
            return allElements.size();
        } catch (Exception e) {
            throw new StockFileManipulateException("Csv File read/write exception", e);
        }
    }

    public static <T extends CsvBean> int deleteRecordInCsvFile(String filePath, int line) {
        try {
            Reader reader = new FileReader(filePath);
            CSVReader csvReader = new CSVReader(reader);
            List<String[]> allElements = csvReader.readAll();
            allElements.remove(line);
            reader.close();

            Writer writer = new FileWriter(filePath);
            CSVWriter csvWriter = new CSVWriter(writer);
            csvWriter.writeAll(allElements);
            writer.close();
            return allElements.size();
        } catch (Exception e) {
            throw new StockFileManipulateException("Csv File write/read exception", e);
        }
    }

}
