package org.qtcoding.stock.demo.common;

import org.qtcoding.stock.demo.exception.StockFileManipulateException;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;

public class InvokeGetterSetter {
    public static void invokeSetter(Object obj, String propertyName, Object variableValue) {
        PropertyDescriptor pd;
        try {
            pd = new PropertyDescriptor(propertyName, obj.getClass());
            Method setter = pd.getWriteMethod();
            setter.invoke(obj, variableValue);
        } catch (Exception e) {
            throw new StockFileManipulateException("", e);
        }
    }

    public static Object invokeGetter(Object obj, String propertyName) {
        PropertyDescriptor pd;
        try {
            pd = new PropertyDescriptor(propertyName, obj.getClass());
            Method getter = pd.getReadMethod();
            return getter.invoke(obj);
        } catch (Exception e) {
            throw new StockFileManipulateException("", e);
        }
    }
}
